package android.washingtonk.reddit;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class RedditPostTask extends AsyncTask<RedditListFragment, Void, JSONObject> {
    private RedditListFragment redditListFragment;
    @Override
    protected JSONObject doInBackground(RedditListFragment... redditListFragments) {
        JSONObject jsonObject = null;
        redditListFragment = redditListFragments[0];
        try {
            URL redditFeedUrl = new URL("https://reddit.com/r/android.json");

            HttpURLConnection httpURLConnection = (HttpURLConnection)redditFeedUrl.openConnection();
            httpURLConnection.connect();

            int statusCode = httpURLConnection.getResponseCode();
            /* checks wether or not the staus code is equal to 200 or not*/
            if(statusCode == HttpURLConnection.HTTP_OK) {
                //parse through input stream
                jsonObject = RedditPostParser.getInstance().parseInputStream(httpURLConnection.getInputStream());

            }
        }
        catch(MalformedURLException error){
            Log.e("RedditPostTask", "MalformedURLException (doInBackground): " + error);
        }
        catch(IOException error) {
            Log.e("RedditPostTask", "IOException (doInBackground) : " + error);
        }

        return jsonObject;
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        RedditPostParser.getInstance().readRedditFeed(jsonObject);
        redditListFragment.updateUserInterface();

    }
}
