package android.washingtonk.reddit;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.util.List;

public class RedditWebActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private List<RedditPost> redditPosts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reddit_web);
        viewPager =(ViewPager)findViewById(R.id.viewPager);
        redditPosts = RedditPostParser.getInstance().redditPosts;

        Intent intent = getIntent();
        Uri redditUri = intent.getData();

        FragmentManager fragmentManager = getSupportFragmentManager();
        viewPager.setAdapter(new FragmentStatePagerAdapter(fragmentManager) {
            @Override
            public Fragment getItem(int position) {
                RedditPost redditPost = redditPosts.get(position);
                return RedditWebFragment.newFragment(redditPost.url);

            }

            @Override
            public int getCount() {
                return redditPosts.size();
            }
        });

        for (int index =0; index < redditPosts.size(); index++) {
            if (redditPosts.get(index).url.equals(redditUri.toString())) {
                viewPager.setCurrentItem(index);
                break;

            }
        }
    }
}
