package android.washingtonk.reddit;

import android.net.Uri;

public interface ActivityCallback {
    void onPostSelected(Uri redditPostUri);
}
